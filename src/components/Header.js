import React from 'react';


function Header () {
    return (
        <nav className="navbar navbar-expand-md navbar-dark navbar-default navbar-static-top bg-dark">
           <h1 className='header'>News</h1>
        </nav>
    );
}

export default Header;
import React from "react";
import moment from 'moment';
import {pinArticle, unpinArticle} from "../actions/articleActions";
import {connect} from "react-redux";


class Article extends React.Component {
    constructor(props) {
        super(props);

        this.articleId = decodeURIComponent(this.props.match.params.id);

        this.state = {
            thumbnail: "",
            headline: "",
            content: ""
        };
    }

    handlePin() {
        if (this.isPinned()) {
            this.props.unpinArticle(this.articleId);
        } else {
            this.props.pinArticle(this.props.articles.find(a => a.id === this.articleId));
        }

        this.props.history.goBack();
    }

    isPinned() {
        return !!this.props.pins.find(p => p.id === this.articleId);
    }

    componentDidMount() {
        fetch(`https://content.guardianapis.com/${this.articleId}?api-key=b37567aa-6c1a-4a0c-a673-9f347a6ba629&show-fields=all`)
            .then(response => response.json())
            .then(data => this.setState({
                thumbnail: data.response.content.fields.thumbnail,
                content: data.response.content.fields.bodyText,
                headline: data.response.content.fields.headline,
                byline: data.response.content.fields.byline,
                publicationDate: data.response.content.fields.publicationDate,
                originalUrl: data.response.content.webUrl,
            }));
    }

    render() {
        return (
            <div className="container article">
                <div className="col-xl-12 mt-2">
                    <img src={this.state.thumbnail} className="rounded float-left img-thumbnail mr-3" alt=''/>
                    <h3 className="mb-5">{this.state.headline}</h3>
                    <button type="button" className="btn btn-info btn-sm" onClick={this.handlePin.bind(this)}>
                        {this.isPinned() ? "Unpin" : "Pin"}
                    </button>
                    <a role="button" target="_blank" rel="noopener noreferrer" href={this.state.originalUrl}
                       className="btn btn-outline-primary ml-1 btn-sm">Open Original</a>
                    <p className="mb-0">
                        <small>By <em>{this.state.byline}</em></small>
                    </p>
                    <p>
                        <small>{moment(this.state.publicationDate).format('MMMM Do YYYY, h:mm:ss a')}</small>
                    </p>
                    <p className="content">{this.state.content}</p>
                </div>
            </div>)
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        pinArticle: articleId => dispatch(pinArticle(articleId)),
        unpinArticle: articleId => dispatch(unpinArticle(articleId)),
    }
};

const mapStateToProps = (state) => {
    return {
        articles: state.articles,
        pins: state.pins,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Article);

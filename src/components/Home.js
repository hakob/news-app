import React from 'react';
import Pin from "./PinContainer";
import {Link} from 'react-router-dom';
import InfiniteScroll from 'react-infinite-scroller';
import moment from 'moment';
import {viewArticle, appendArticles, addNewArticle} from "../actions/articleActions";
import {connect} from 'react-redux';

function ArticleItem(article, index, isRead, isNew, {handleRead}) {
    let image = article.fields.thumbnail || require('../no-image.jpg');

    return (
        <div className="news-card " key={index}>
            <div className="news-card card">
                <img className="card-img-top" src={image} alt=""/>
                <div className={"card-body " + (isNew ? "new-animation" : "")}>
                    <h5 className="card-title text-truncate">{article.fields.headline}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">{article.sectionName}</h6>
                    <h6 className="card-subtitle mt-2 font-weight-light">
                        <small>{moment(article.webPublicationDate).format('MMMM Do YYYY, h:mm:ss a')}</small>
                    </h6>
                    {isRead ? <span className="badge badge-secondary position-absolute view-badge">Viewed</span> : null}
                    {isNew ?
                        <span className="badge badge-primary position-absolute new-badge">New</span> : null}
                </div>
                <div className="card-footer">
                    <div className="clearfix">
                        <Link to={"/article/" + encodeURIComponent(article.id)} className="btn btn-primary btn-block"
                              onClick={() => {
                                  handleRead(article.id)
                              }}>Read more</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}


class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showNotification: false,
            newAdded: []
        };
        this.addPin = this.addPin.bind(this);
        this.handleRead = this.handleRead.bind(this);
        this.newRef = React.createRef();

    }


    addPin(data) {
        this.setState(prevState => ({
            pins: [...prevState.pins, data]
        }))
    }

    handleRead(i) {
        this.props.viewArticle(i);
        this.setState(prevState => ({
            read: [...prevState.read, i]
        }));
    }

    persistState() {
        localStorage.setItem('read', JSON.stringify(this.state.read));
    }

    loadState() {
        let reads = JSON.parse(localStorage.getItem('read'));

        if (reads) {
            this.setState({read: reads});
        }
    }


    componentDidMount() {
        this.loadState();

        // check for new articles
        setInterval(this.loadNewArticles.bind(this), 30 * 1000);
    }

    loadArticles(page) {
        if (this.fetching) {
            return;
        }

        this.fetching = true;

        fetch(`https://content.guardianapis.com/search?page=${page}&page-size=30&show-fields=headline,thumbnail&order-by=newest&api-key=b37567aa-6c1a-4a0c-a673-9f347a6ba629`)
            .then(response => response.json())
            .then(data => {
                this.fetching = false;
                this.props.appendArticles([...data.response.results]);
            });
    }

    loadNewArticles() {
        fetch(`https://content.guardianapis.com/search?page=1&page-size=30&show-fields=headline,thumbnail&order-by=newest&api-key=b37567aa-6c1a-4a0c-a673-9f347a6ba629`)
            .then(response => response.json())
            .then(data => {
                let r = data.response.results;
                // if any new articles ?
                let latest = this.props.articles[0];
                r = r.filter(a => a.webPublicationDate > latest.webPublicationDate);

                // check if no copy articles in current list
                let currentArticles = new Set(this.props.articles.map(a=>a.id));
                r =r.filter(a=>!currentArticles.has(a));

                this.setState({
                   newAdded: r.map(a=>a.id)
                });

                if (r.length > 0) {
                    // new articles
                    for (let a of r) {
                        this.props.addNewArticle(a);
                    }
                    this.setState({
                        showNotification: true
                    });
                }
            });
    }

    clickNotification() {
        window.scrollTo(0, this.newRef.offsetTop);

        this.setState({
            showNotification: false
        })
    }

    render() {
        let pins = this.props.pins.map(p=>p.id);
        let articles = this.props.articles.filter(a=>!pins.includes(a.id)).map((article, i) => {
            let isRead = this.props.views.includes(article.id);

            return ArticleItem(article, i, isRead, this.state.newAdded.includes(article.id), {
                handleRead: this.handleRead.bind(this)
            });
        });

        return (
            <div className="text-center container">
                <Pin data={this.props.pins}/>
                <hr style={{
                    color: 'black',
                    height: 3,
                    backgroundColor: 'black',
                    marginTop: 50

                }}/>
                <h1>Articles</h1>

                {
                    this.state.showNotification ?
                        <div className="alert alert-info position-fixed fixed-top notification" role="alert"
                             onClick={this.clickNotification.bind(this)}>
                            You have new items check it. Click to see.
                        </div> : null
                }


                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadArticles.bind(this)}
                    hasMore={true}>

                    <div className="card-deck" ref={this.newRef}>
                        {articles}
                    </div>
                </InfiniteScroll>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        viewArticle: articleId => dispatch(viewArticle(articleId)),
        addNewArticle: article => dispatch(addNewArticle(article)),
        appendArticles: articles => dispatch(appendArticles(articles)),
    }
};

const mapStateToProps = (state) => {
    return {
        articles: state.articles,
        pins: state.pins,
        views: state.views,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

import React from "react";
import moment from "moment";
import {Link} from "react-router-dom";

class Pin extends React.Component {
    render() {
        return (
            <div className="card-deck mt-2" id="pin">
                {(this.props.data.map(function (article) {
                    let image = article.fields.thumbnail || require('../no-image.jpg');

                    return (
                        <div className="pin-card ">
                            <div className="pin-card card">
                                <img className="card-img-top" src={image} alt=""/>
                                <div className="card-body">
                                    <h5 className="card-title text-truncate">{article.fields.headline}</h5>
                                    <h6 className="card-subtitle mb-2 text-muted">{article.sectionName}</h6>
                                    <h6 className="card-subtitle mt-2 font-weight-light">
                                        <small>{moment(article.webPublicationDate).format('MMMM Do YYYY, h:mm:ss a')}</small>
                                    </h6>
                                </div>
                                <div className="card-footer">
                                    <div className="clearfix">
                                        <Link to={"/article/" + encodeURIComponent(article.id)} className="btn btn-primary btn-block">Read more</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }))}
            </div>
        )
    }
}


export default Pin;
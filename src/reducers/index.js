import {combineReducers} from 'redux';
import articles from './articleReducer';
import pins from './pinedReducer';
import views from './viewReducer';

export default combineReducers({
    views,
    articles,
    pins
});
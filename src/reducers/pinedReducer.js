import * as actionTypes from '../actions/types';

export default (state = [], action) => {
    switch (action.type) {
        case actionTypes.PIN_ARTICLE:
            return [
                ...state,
                Object.assign({}, action.article)
            ];

        case actionTypes.UNPIN_ARTICLE:
            return state.filter(a => a.id !== action.articleId);

        default:
            return state;
    }
};
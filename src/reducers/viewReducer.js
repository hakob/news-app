import * as actionTypes from '../actions/types';

export default (state = [], action) => {
    switch (action.type){
        case actionTypes.VIEW_ARTICLE:
            return [
                ...state,
               action.articleId
            ];

        default:
            return state;
    }
};
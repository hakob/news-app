import * as actionTypes from '../actions/types';

export default (state = [], action) => {
    switch (action.type) {
        case actionTypes.ADD_NEW_ARTICLE:
            return [
                Object.assign({}, action.article),
                ...state
            ];

        case actionTypes.APPEND_ARTICLES:
            return [
                ...state,
                ...action.articles
            ];
        default:
            return state;
    }
};
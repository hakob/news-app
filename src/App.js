import React from 'react';
import Main from './routes'
import Header from './components/Header'
import './App.css';

function App() {
  return (
    <div className="App">
        <Header/>
      <Main/>
    </div>
  );
}

export default App;

import React from 'react';
import {Switch, Route} from 'react-router-dom'
import Home from './components/Home';
import Article from "./components/Article";


const Main = () => (
    <main>
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/home' component={Home}/>
            <Route path='/article/:id' component={Article}/>
        </Switch>
    </main>
);

export default Main

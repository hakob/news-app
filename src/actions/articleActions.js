import * as types from './types';

export const viewArticle = (articleId) => {
    return {
        type: types.VIEW_ARTICLE,
        articleId: articleId,
    }
};

export const pinArticle = (article) => {
    return {
        type: types.PIN_ARTICLE,
        article: {...article},
    }
};

export const unpinArticle = (articleId) => {
    return {
        type: types.UNPIN_ARTICLE,
        articleId: articleId,
    }
};


export const addNewArticle = (article) => {
    return {
        type: types.ADD_NEW_ARTICLE,
        article: {...article},
    }
};

export const appendArticles = (articles) => {
    return {
        type: types.APPEND_ARTICLES,
        articles: [...articles],
    }
};